#!/bin/bash

MAX_SIZE=32767	# Range 0-32767
MAX_FILE_COUNT=$(( 32767 * 32767 ))	# Range 0-1073676289
PROCESS_NUMBER=3
STATUS_REFRESH_TIMEOUT=2
CHECKSUM=sha1sum	# Avaliable: md5sum sha1sum sha224sum sha256sum sha384sum sha512sum shasum

# Action's weight.
WRITE_WEIGHT=5
READ_WEIGHT=5
REMOVE_WEIGHT=4

DEBUG_ON=0
EMPTY_AFTER_TEST=1

#TMP_DIR=/tmp/rw-test
TMP_DIR=`mktemp -u /tmp/rw-test.XXXXXXXX`
if [[ -z "${TMP_DIR}" ]]; then
	TMP_DIR="/tmp/rw-test"
fi

