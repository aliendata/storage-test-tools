package main

import (
	"fmt"
	"os"
	"bufio"
	"strings"
	"rwtest/typedef"
)

func printHelp() {
	fmt.Println("Start this tool:")
	fmt.Println("  ", os.Args[0], "<TestDir>")
	fmt.Println("    <TestDir> must be an empty dir,")
	fmt.Println("    which will be write a lot of little files.")
	fmt.Println("")
	fmt.Println("Command line view:")
	fmt.Println("  start\tStart all threads.")
	fmt.Println("  stop\tStop all threads.")
	fmt.Println("  restart\tRestart all threads.")
	fmt.Println("  exit\tStop all threads, and exit this program.")
	fmt.Println("  status\tShow the status view.")
	fmt.Println("  set <key> = <value>\tSet a value to env set.")
	fmt.Println("  get <key>\tGet a value from env set.")
	fmt.Println("  unset <key>\tSet the value of key to 0 or empty string.")
	fmt.Println("  env\tPrint all env.")
	fmt.Println("  clear\tClear screen.")
	fmt.Println("  help\tPrint this help text.")
	fmt.Println("")
	fmt.Println("Status view:")
	fmt.Println("  Print current status to screen.")
	fmt.Println("  To quit this view, just type \"q\" or \"quit\", and press EMTER.")
	fmt.Println("")
}

func FuncMain(pcfg *typedef.CfgStruct) {
	var (
		cfg *typedef.CfgStruct
		stat *typedef.Status
		threadpool *typedef.GoThreadPool = &typedef.GoThreadPool{}
		cmdReader *bufio.Reader = bufio.NewReader(os.Stdin)
		err error = nil
		cmd string = ""
		cmds []string = make([]string, 0, 4)
	)
	cfg = pcfg
	stat = &cfg.Stat

	// start goroutine pool
	threadpool.Init(cfg)
	threadpool.Start()

	// please implement "Console" here
	console_event:
	for {
		fmt.Print("= command => ")
		cmd, err = cmdReader.ReadString('\n')
		if err != nil {
			fmt.Println("Illegal command.")
		}
		cmd = strings.TrimSpace(cmd)
		cmds = strings.Split(cmd, " ")
		if cmd == "start" {
			threadpool.Start()
		} else if cmd == "stop" {
			threadpool.Stop()
		} else if cmd == "restart" {
			threadpool.Stop()
			threadpool.Start()
		} else if cmd == "exit" {
			threadpool.Stop()
			break console_event
		} else if cmd == "status" {
			stat.ShowStatus(cfg.Number["STATUS_REFRESH_TIMEOUT"])
			loop_status:
			for {
				cmd, _ = cmdReader.ReadString('\n')
				cmd = strings.TrimSpace(cmd)
				if cmd == "q" || cmd == "quit" {
					stat.ExitStatus()
					break loop_status
				}
			}
		} else if cmds[0] == "set" {
			cmd = ""
			cmds = cmds[1:]
			for _, str := range(cmds) {
				cmd = cmd + str
			}
			cmds = strings.Split(cmd, "=")
			if len(cmds) != 2 {
				fmt.Println("Illegal command.")
				fmt.Println("Usage: set <key> = <value>")
				continue
			}
			cfg.Set(cmds[0], cmds[1])
		} else if cmds[0] == "get" {
			cmds = cmds[1:]
			if len(cmds) != 1 {
				fmt.Println("Illegal command.")
				fmt.Println("Usage: get <key>")
				continue
			}
			fmt.Println(cfg.GetToStr(cmds[0]))
		} else if cmds[0] == "unset" {
			cmds = cmds[1:]
			if len(cmds) != 1 {
				fmt.Println("Illegal command.")
				fmt.Println("Usage: unset <key>")
				continue
			}
			cfg.Delete(cmds[0])
			fmt.Println(cfg.GetToStr(cmds[0]))
		} else if cmd == "env" {
			cfg.ShowEnv()
		} else if cmd == "clear" {
			typedef.ClearScreen()
		} else if cmd == "help" || cmd == "usage" {
			printHelp()
		} else if cmd == "" {
			continue
		} else {
			fmt.Println("command not found.")
		}
		cmd = ""
	}
}

